import React from 'react'

import { GoogleAuthProvider } from 'firebase/auth'
import { CssBaseline, ThemeProvider } from '@mui/material'
import { BrowserRouter as Router } from 'react-router-dom'

import 'typeface-rubik'
import 'typeface-space-mono'

import {
  AuthDelegate,
  Authenticator,
  buildCollection,
  buildProperty,
  buildSchema,
  CircularProgressCenter,
  CMSView,
  createCMSDefaultTheme,
  EntityCustomView,
  EntityReference,
  FirebaseLoginView,
  FireCMS,
  NavigationBuilder,
  NavigationBuilderProps,
  NavigationRoutes,
  Scaffold,
  SideEntityDialogs,
  useFirebaseAuthDelegate,
  useFirebaseStorageSource,
  useFirestoreDataSource,
  useInitialiseFirebase,
} from '@camberi/firecms'
import { ExampleCMSView } from './ExampleCMSView'

const DEFAULT_SIGN_IN_OPTIONS = [GoogleAuthProvider.PROVIDER_ID]
// TODO: Replace with your config
export const firebaseConfig = {
  apiKey: 'AIzaSyDS8I_ZRuZLIKcvsP6FLDNIaHCd9QFw-ek',
  authDomain: 'capoeira-rosenheim.firebaseapp.com',
  projectId: 'capoeira-rosenheim',
  storageBucket: 'capoeira-rosenheim.appspot.com',
  messagingSenderId: '351380832899',
  appId: '1:351380832899:web:3aafdd26848f8c25f511b6',
  measurementId: 'G-NK877XQHN7',
}

const locales = {
  'en-US': 'English (United States)',
  'es-ES': 'Spanish (Spain)',
  'de-DE': 'German',
}

type Product = {
  name: string
  price: number
  status: string
  published: boolean
  related_products: EntityReference[]
  main_image: string
  tags: string[]
  description: string
  categories: string[]
  publisher: {
    name: string
    external_id: string
  }
  expires_on: Date
}

const sampleView: EntityCustomView<Product> = {
  path: 'preview',
  name: 'Blog entry preview',
  builder: ({ schema, entity, modifiedValues }) => {
    console.log('entity', entity)
    console.log('modifiedValues', modifiedValues)
    return (
      // This is a custom component that you can build as any React component
      <div>
        <strong>{entity?.values.name}</strong>
        <strong>{modifiedValues?.name}</strong>
      </div>
    )
  },
}

const productSchema = buildSchema<Product>({
  name: 'Product',
  views: [sampleView],
  properties: {
    name: {
      title: 'Name',
      validation: { required: true },
      dataType: 'string',
    },
    price: {
      title: 'Price',
      validation: {
        required: true,
        requiredMessage: 'You must set a price between 0 and 1000',
        min: 0,
        max: 1000,
      },
      description: 'Price with range validation',
      dataType: 'number',
    },
    status: {
      title: 'Status',
      validation: { required: true },
      dataType: 'string',
      description: 'Should this product be visible in the website',
      longDescription:
        'Example of a long description hidden under a tooltip. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis bibendum turpis. Sed scelerisque ligula nec nisi pellentesque, eget viverra lorem facilisis. Praesent a lectus ac ipsum tincidunt posuere vitae non risus. In eu feugiat massa. Sed eu est non velit facilisis facilisis vitae eget ante. Nunc ut malesuada erat. Nullam sagittis bibendum porta. Maecenas vitae interdum sapien, ut aliquet risus. Donec aliquet, turpis finibus aliquet bibendum, tellus dui porttitor quam, quis pellentesque tellus libero non urna. Vestibulum maximus pharetra congue. Suspendisse aliquam congue quam, sed bibendum turpis. Aliquam eu enim ligula. Nam vel magna ut urna cursus sagittis. Suspendisse a nisi ac justo ornare tempor vel eu eros.',
      config: {
        enumValues: {
          private: 'Private',
          public: 'Public',
        },
      },
    },
    published: ({ values }) =>
      buildProperty({
        title: 'Published',
        dataType: 'boolean',
        columnWidth: 100,
        disabled:
          values.status === 'public'
            ? false
            : {
                clearOnDisabled: true,
                disabledMessage: 'Status must be public in order to enable this the published flag',
              },
      }),
    related_products: {
      dataType: 'array',
      title: 'Related products',
      description: 'Reference to self',
      of: {
        dataType: 'reference',
        path: 'websites/capoeira/products',
      },
    },
    main_image: buildProperty({
      // The `buildProperty` method is an utility function used for type checking
      title: 'Image',
      dataType: 'string',
      config: {
        storageMeta: {
          mediaType: 'image',
          storagePath: 'images',
          acceptedFiles: ['image/*'],
        },
      },
    }),
    tags: {
      title: 'Tags',
      description: 'Example of generic array',
      validation: { required: true },
      dataType: 'array',
      of: {
        dataType: 'string',
      },
    },
    description: {
      title: 'Description',
      description: "Not mandatory but it'd be awesome if you filled this up",
      longDescription:
        'Example of a long description hidden under a tooltip. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis bibendum turpis. Sed scelerisque ligula nec nisi pellentesque, eget viverra lorem facilisis. Praesent a lectus ac ipsum tincidunt posuere vitae non risus. In eu feugiat massa. Sed eu est non velit facilisis facilisis vitae eget ante. Nunc ut malesuada erat. Nullam sagittis bibendum porta. Maecenas vitae interdum sapien, ut aliquet risus. Donec aliquet, turpis finibus aliquet bibendum, tellus dui porttitor quam, quis pellentesque tellus libero non urna. Vestibulum maximus pharetra congue. Suspendisse aliquam congue quam, sed bibendum turpis. Aliquam eu enim ligula. Nam vel magna ut urna cursus sagittis. Suspendisse a nisi ac justo ornare tempor vel eu eros.',
      dataType: 'string',
      columnWidth: 300,
    },
    categories: {
      title: 'Categories',
      validation: { required: true },
      dataType: 'array',
      of: {
        dataType: 'string',
        config: {
          enumValues: {
            electronics: 'Electronics',
            books: 'Books',
            furniture: 'Furniture',
            clothing: 'Clothing',
            food: 'Food',
          },
        },
      },
    },
    publisher: {
      title: 'Publisher',
      description: 'This is an example of a map property',
      dataType: 'map',
      properties: {
        name: {
          title: 'Name',
          dataType: 'string',
        },
        external_id: {
          title: 'External id',
          dataType: 'string',
        },
      },
    },
    expires_on: {
      title: 'Expires on',
      dataType: 'timestamp',
    },
  },
})

const localeSchema = buildSchema({
  customId: locales,
  name: 'Locale',
  properties: {
    title: {
      title: 'Title',
      validation: { required: true },
      dataType: 'string',
    },
    selectable: {
      title: 'Selectable',
      description: 'Is this locale selectable',
      dataType: 'boolean',
    },
    video: {
      title: 'Video',
      dataType: 'string',
      validation: { required: false },
      config: {
        storageMeta: {
          mediaType: 'video',
          storagePath: 'videos',
          acceptedFiles: ['video/*'],
        },
      },
    },
  },
})
/**
 * This is an example of how to use the components provided by FireCMS for
 * a better customisation.
 */
const CustomCMSApp = () => {
  const customViews: CMSView[] = [
    {
      path: ['additional', 'additional/:id'],
      name: 'Additional view',
      description: 'This is an example of an additional view that is defined by the user',
      // This can be any React component
      view: <ExampleCMSView />,
    },
  ]
  const navigation: NavigationBuilder = ({ user }: NavigationBuilderProps) => ({
    collections: [
      buildCollection({
        path: 'websites/capoeira/products',
        schema: productSchema,
        name: 'Products',
        permissions: ({ user }) => ({
          edit: true,
          create: true,
          delete: true,
        }),
      }),
    ],
    views: customViews,
  })

  const myAuthenticator: Authenticator = ({ user }) => {
    console.log('Allowing access to', user?.email)
    return true
  }

  const { firebaseApp, firebaseConfigLoading, configError, firebaseConfigError } =
    useInitialiseFirebase({ firebaseConfig })

  const authDelegate: AuthDelegate = useFirebaseAuthDelegate({
    firebaseApp,
  })

  const dataSource = useFirestoreDataSource({
    firebaseApp: firebaseApp,
    // You can add your `FirestoreTextSearchController` here
  })
  const storageSource = useFirebaseStorageSource({ firebaseApp: firebaseApp })

  if (configError) {
    return <div> {configError} </div>
  }

  if (firebaseConfigError) {
    return (
      <div>
        It seems like the provided Firebase config is not correct. If you are using the credentials
        provided automatically by Firebase Hosting, make sure you link your Firebase app to Firebase
        Hosting.
      </div>
    )
  }

  if (firebaseConfigLoading || !firebaseApp) {
    return <CircularProgressCenter />
  }

  return (
    <Router>
      <FireCMS
        navigation={navigation}
        authentication={myAuthenticator}
        authDelegate={authDelegate}
        dataSource={dataSource}
        storageSource={storageSource}
        entityLinkBuilder={({ entity }) =>
          `https://console.firebase.google.com/project/${firebaseApp.options.projectId}/firestore/data/${entity.path}/${entity.id}`
        }
      >
        {({ context, mode, loading }) => {
          const theme = createCMSDefaultTheme({ mode })

          let component
          if (loading) {
            component = <CircularProgressCenter />
          } else if (!context.authController.canAccessMainView) {
            component = (
              <FirebaseLoginView
                skipLoginButtonEnabled={false}
                signInOptions={DEFAULT_SIGN_IN_OPTIONS}
                firebaseApp={firebaseApp}
                authDelegate={authDelegate}
              />
            )
          } else {
            component = (
              <Scaffold name={'My Online Shop'}>
                <NavigationRoutes />
                <SideEntityDialogs />
              </Scaffold>
            )
          }

          return (
            <ThemeProvider theme={theme}>
              <CssBaseline />
              {component}
            </ThemeProvider>
          )
        }}
      </FireCMS>
    </Router>
  )
}

export default CustomCMSApp
